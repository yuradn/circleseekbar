package com.cookies.customview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    @OnClick(R.id.btnCustom)
    protected void runCustomActivity(){
        Intent intent = new Intent(this, CustomActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnSimple)
    protected void runSimpleActivity(){
        Intent intent = new Intent(this, SimpleActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }


}
